YAML form options token
-----------------------

Provides token support for YAML form options.

Example
-------

    example:
      '#type': radios
      '#title': Example
      '#options':
        1: '[current-date:html_datetime]'
        2: '[current-user:uid]'
        3: '[site:name]'
